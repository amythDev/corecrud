﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreCrud.Data;
using Microsoft.EntityFrameworkCore;
using CoreCrud.Model;

namespace CoreCrud.Controllers
{
    public class HomeController : Controller
    {
        #region declaration
        private readonly EmployeeContext _employeeContext;
        #endregion

        #region initialization
        //Context added through inbuilt dependency injection (see startup.cs)
        public HomeController(EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
        }
        #endregion

        #region methods

        /// <summary>
        /// displays the Application default page
        /// </summary>
        /// <returns>View</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Returns list of sll the students
        /// </summary>
        /// <returns>view</returns>
        public async Task<IActionResult> Listing()
        {
            return View(await _employeeContext.Employee.OrderBy(x=>x.EmployeeFirstName).AsNoTracking().ToListAsync());
        }


        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([Bind(include: "EmployeeFirstName, EmployeeLastName")] Employee employee  )
        {
            try
            {
                if(ModelState.IsValid)
                {
                    await _employeeContext.Employee.AddAsync(employee);
                    await _employeeContext.SaveChangesAsync();
                    return RedirectToAction("Listing");
                }
            }catch(Exception e)
            {
                ModelState.AddModelError("", "Unable to Save employee Data");
            }
            return View(employee);
        }


        /// <summary>
        /// extract details of a particular employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet]
         public async Task<IActionResult> Detail(int employeeId)
          {
            try
            {
                Employee emp = await _employeeContext.Employee.FirstAsync(x => x.EmployeeId == employeeId);
                if (emp != null)
                {
                    return View(emp);
                }
                else
                {
                    return BadRequest();
                }
            }catch(Exception e)
            {
                return BadRequest();
            }
          }

        /// <summary>
        /// get data of employee to be edited
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Edit (int employeeId)
        {
            try
            {
                Employee emp = await _employeeContext.Employee.FirstAsync(x => x.EmployeeId == employeeId);
                if (emp != null)
                {
                    return View(emp);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// saved the updated data
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Edit([Bind(include: "EmployeeId,EmployeeFirstName, EmployeeLastName")] Employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   Employee emp = await _employeeContext.Employee.FirstAsync(x => x.EmployeeId == employee.EmployeeId);
                    emp.EmployeeFirstName = employee.EmployeeFirstName;
                    emp.EmployeeLastName = employee.EmployeeLastName;
                    await _employeeContext.SaveChangesAsync();
                    return RedirectToAction("Listing");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Unable to Update employee Data");
            }
            return View(employee);
        }
       

        
        #endregion
    }
}
